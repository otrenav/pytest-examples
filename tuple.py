
class Tuple(object):

    v = 1  # Class variable

    def __init__(self, x, y, w, z):
        # Instance variables
        self._x_internal  = x
        self._y_internal  = y
        self._w  = w
        self.__z = z

    def __str__(self):
        return(
            " _x_internal: " + str(self._x_internal) + " (variable)" +
            "\n _y_internal: " + str(self._y_internal) + " (variable)" +
            "\n _x: " + str(self._x) + " (property)" +
            "\n _y: " + str(self._y) + " (property)" +
            "\n _w: " + str(self._w) + " (variable)" +
            "\n__z: " + str(self.__z) + " (variable)"
        )

    ##
    ## Class method
    ##
    @staticmethod
    def get_sum_class():
        return(10 + 4 + Tuple.v)

    ##
    ## Instance methods
    ##
    def get_sum(self):
        return(self._x_internal + self._y_internal)

    def get_product(self):
        return(self._x_internal*self._y_internal)

    ##
    ## Properties
    ##
    @property
    def _x(self):
        return(self._x_internal)

    @_x.setter
    def _x(self, value):
        print("Can't set value")

    @_x.deleter
    def _x(self, value):
        print("Can't delete value")

    @property
    def _y(self):
        return(self._y_internal)

    @_y.setter
    def _y(self, value):
        print("Can't set value")

    @_y.deleter
    def _y(self, value):
        print("Can't delete value")

    ##
    ## Properties can me complex
    ##
    @property
    def sum(self):
        return(self._x_internal + self._y_internal)

    @sum.setter
    def sum(self, value):
        print("Can't set values")

    @sum.deleter
    def sum(self):
        print("Can't delete values")

    @property
    def product(self):
        return(self._x_internal*self._y_internal)

    @product.setter
    def product(self, value):
        print("Can't set values")

    @product.deleter
    def product(self):
        print("Can't delete values")


if __name__ == "__main__":

    print("\nClass methods don't need instantiation")
    print(Tuple.get_sum_class())

    print("\nGet an instance")
    print("--------------")
    t = Tuple(2, 3, 4, 5)
    print(t)

    print("\nGet values")
    print("-----------")
    print("_x_internal: " + str(t._x_internal) + " (variable)")
    print("_y_internal: " + str(t._y_internal) + " (variable)")
    print("_x: "  + str(t._x)  + " (property)")
    print("_y: "  + str(t._y)  + " (property)")
    print("_w: "  + str(t._w)  + " (variable)")
    try:
        print("__z: " + str(t.__z) + " (variable)")
    except AttributeError as e:
        print(e)
    print("_Tuple__z: " + str(t._Tuple__z) + " (variable)")

    print("\n(Note: we CAN NOT see __z)")
    print("(Note: but we CAN see it with _Tuple__z)")

    print("\nTry to set values")
    print("-----------------")
    print("t._x_internal  = 3")
    t._x_internal  = 3
    print("t._y_internal  = 4")
    t._y_internal  = 4
    print("t._x  = 3")
    t._x  = 3
    print("t._y  = 4")
    t._y  = 4
    print("t._w  = 5")
    t._w  = 5
    print("t.__z = 10")
    t.__z = 10
    print("\n(Note: PROTECTION from overriding properties)")
    print("(Note: NO PROTECTION from overriding __z)")

    print("\nGet values")
    print("-----------")
    print("_x_internal: " + str(t._x_internal) + " (variable)")
    print("_y_internal: " + str(t._y_internal) + " (variable)")
    print("_x: "  + str(t._x)  + " (property)")
    print("_y: "  + str(t._y)  + " (property)")
    print("_w: "  + str(t._w)  + " (variable)")
    print("__z: " + str(t.__z) + " (variable)")
    print("\n(Note: we CAN see __z (and it was overriden))")

## More info: http://www.python-course.eu/python3_properties.php
