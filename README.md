
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Examples for testing with `py.test`

- Omar Trejo
- February, 2017

`py.test` is one of the testing frameworks I like best for Python. It's very
flexible and well written. In this repository I show how to test the `tuple.py`
code with tests inside `tests/test_tuple.py`.

---

> "The best ideas are common property."
>
> —Seneca
