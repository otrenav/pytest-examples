
## Requirements:
# - py.test (no confundir con pytest)
# - pytest-xdist

## Usage:
# $ py.test --collect-only      # Don't execute tests, only list them
# $ py.test -x                  # Stop on the first failure
# $ py.test --duration=10       # Slowest 10 durations
# $ py.test --fixtures          # Show available fixtures
# $ py.test -v                  # To show more info on tests
# $ py.test --runxfail          # Force runing @xfail tests
# $ py.test -k "datata or get"  # Tests with "datata" or "get" in their names
# $ py.test -s                  # Do not capture print statements

import pytest

from datetime import datetime, timedelta

from tuple import Tuple

## This can be imported
## from other modules
DEBUG = True
xfail = pytest.mark.xfail
datata_flag = pytest.mark.datata_flag
only_in_debug_mode = pytest.mark.skipif(DEBUG, reason="Don't test this in DEBUG.")


class TestTuple:

    @pytest.fixture()
    def tuple_object(self):
        """Fixtures are functions/methods that will
        be sent as parameters to tests.
        """
        return(Tuple(2, 3, 4, 5))

    def test_get_sum(self, tuple_object):
        assert tuple_object._x + tuple_object._y == tuple_object.get_sum()

    def test_get_product(self, tuple_object):
        assert tuple_object._x*tuple_object._y == tuple_object.get_product()

    def test_sum_class(self):
        expected = 15
        assert expected == Tuple.get_sum_class(), "Should return {0}".format(expected)

    @only_in_debug_mode
    def test_should_be_skipped(self):
        assert False, "Did you change DEBUG?"

    @xfail
    def test_expected_to_fail(self):
        """Whether this passes or not, it won't
        appear as a failed test.
        """
        assert False, "Fails if forced to execute."

    @xfail(run=False)
    def test_this_test_wont_run(self):
        """If this test crashes the system, you can put
        a `@xfail(run=False)` to stop py.test from even
        running the test.
        """
        raise KeyboardInterrupt

    def test_with_datata_in_its_name(self):
        """You can select all tests that contain a string
        in their function/method names with `$ py.test
        -k <string>`. In this case (with negation)
        `$ py.test -k "not datata"`.
        """
        assert True

    @datata_flag
    def test_with_datata_flag(self):
        """You may tell py.test to only execute tests
        with a flag with `$ py.test -m <flag>`, in this
        case `$ py.test -m datata_flag`. To exclude these
        tests use `$ py.test -m "not datata_flag"`.
        """
        assert True

    @xfail(run=False)
    def test_that_if_executed_calls_pdb(self):
        pytest.set_trace()
        assert True

    def test_exception_on_private_variable(self, tuple_object):
        with pytest.raises(AttributeError):
            print(tuple_object.__z)

    def test_no_exception_on_private_variable(self, tuple_object):
        print(tuple_object._Tuple__z)

    def test_no_exception_on_protected_variable(self, tuple_object):
        print(tuple_object._w)

    def test_no_exception_on_properties(self, tuple_object):
        print(tuple_object._x)
        print(tuple_object._y)

    def test_blocked_assignment_on_properties(self, tuple_object):
        x = tuple_object._x
        y = tuple_object._y
        tuple_object._x = "Something else"
        tuple_object._y = "Something else"
        assert x == tuple_object._x
        assert y == tuple_object._y

##
## Automatic markers
##
@datata_flag
class TestDatata:
    """All the tests in this class will inherit
    the `@datata_flag`.
    """

    def test_with_automatic_flag_one(self):
        assert True

    def test_with_automatic_flag_two(self):
        assert True

##
## Teardown functionality
##
@pytest.fixture(scope="function")
def some_fixture(request):
    message = "Setup"

    def some_teardown():
        print(message + "... and teardown.")

    request.addfinalizer(some_teardown)
    return(message)

def test_that_needs_a_fixture(some_fixture):
    print(some_fixture + "... and testing.")


##
## Parametrized fixtures with custom markers
##
@pytest.mark.foo
@pytest.mark.parametrize("test_input, expected", [
    ("3 + 5", 8),
    ("3 + 4", 7),
    pytest.mark.bar(("2 + 4", 6)),
    ("6 * 9", 54),
    ("6 * 8", 48),
])
def test_eval(test_input, expected):
    assert eval(test_input) == expected

##
## Parametrized fixtures with IDs
##

testdata = [
    (datetime(2016, 03, 28), datetime(2016, 03, 29), timedelta(-1)),
    (datetime(2016, 03, 28), datetime(2016, 03, 27), timedelta(1)),
]

@pytest.mark.parametrize("a, b, expected", testdata)
def test_time_distance_1(a, b, expected):
    diff = a - b
    assert diff == expected


@pytest.mark.parametrize(
    "a, b, expected",
    testdata,
    ids=["foreward", "backward"]
)
def test_time_distance_2(a, b, expected):
    diff = a - b
    assert diff == expected
